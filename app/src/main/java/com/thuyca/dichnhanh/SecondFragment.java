package com.thuyca.dichnhanh;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.ml.common.modeldownload.FirebaseModelDownloadConditions;
import com.google.firebase.ml.naturallanguage.FirebaseNaturalLanguage;
import com.google.firebase.ml.naturallanguage.translate.FirebaseTranslateLanguage;
import com.google.firebase.ml.naturallanguage.translate.FirebaseTranslator;
import com.google.firebase.ml.naturallanguage.translate.FirebaseTranslatorOptions;
import com.thuyca.dichnhanh.databinding.FragmentSecondBinding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SecondFragment extends Fragment {

    private FragmentSecondBinding binding;
    private FirebaseTranslator englishGermanTranslator;
    private List<String> list;
    private FirebaseTranslatorOptions options;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentSecondBinding.inflate(inflater, container, false);

        list = getList();
        ArrayAdapter spinnerAdapter = new ArrayAdapter<>(getContext(), R.layout.support_simple_spinner_dropdown_item, list);
        binding.spinnerInSecond.setAdapter(spinnerAdapter);
        binding.spinnerOutSecond.setAdapter(spinnerAdapter);

        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.buttonPreviousSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(SecondFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });

        binding.buttonNextSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = binding.edittextSecond.getText().toString();
                downloadModal(text);
                NavHostFragment.findNavController(SecondFragment.this)
                        .navigate(R.id.action_SecondFragment_to_ThirdFragment);
            }
        });

//        binding.spinnerInSecond.setOnClickListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                Log.d("ABC", "onItemSelected: " + i);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//                Log.d("ABC", "onNothingSelected: ");
//            }
//        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private void downloadModal(String input) {
        // below line is use to download the modal which
        // we will require to translate in german language
        options = new FirebaseTranslatorOptions.Builder()
                // below line we are specifying our source language.
                .setSourceLanguage(binding.spinnerInSecond.getSelectedItemPosition())
                // in below line we are displaying our target language.
                .setTargetLanguage(binding.spinnerOutSecond.getSelectedItemPosition())
                // after that we are building our options.
                .build();
        englishGermanTranslator = FirebaseNaturalLanguage.getInstance().getTranslator(options);

        FirebaseModelDownloadConditions conditions = new FirebaseModelDownloadConditions.Builder().requireWifi().build();

        // below line is use to download our modal.
        englishGermanTranslator.downloadModelIfNeeded(conditions).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

                // this method is called when modal is downloaded successfully.
//                Toast.makeText(MainActivity.this, "Please wait language modal is being downloaded.", Toast.LENGTH_SHORT).show();

                // calling method to translate our entered text.
                translateLanguage(input);

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
//                Toast.makeText(MainActivity.this, "Fail to download modal", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void translateLanguage(String input) {
        englishGermanTranslator.translate(input).addOnSuccessListener(new OnSuccessListener<String>() {
            @Override
            public void onSuccess(String s) {
//                translateLanguageTV.setText(s);
                ThirdFragment fragment = new ThirdFragment();
                Bundle bundle = new Bundle();
                bundle.putString("string", s);

                fragment.setArguments(bundle);

                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.nav_host_fragment_content_main , fragment);

                fragmentTransaction.commit();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
//                Toast.makeText(MainActivity.this, "Fail to translate", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private List<String> getList(){
        List<String> stringList = new ArrayList<>();
        stringList.add("AF");
        stringList.add("AR");
        stringList.add("BE");
        stringList.add("BG");
        stringList.add("BN");
        stringList.add("CA");
        stringList.add("CS");
        stringList.add("CY");
        stringList.add("DA");
        stringList.add("DE");
        stringList.add("EL");
        stringList.add("EN");
        stringList.add("EO");
        stringList.add("ES");
        stringList.add("ET");
        stringList.add("FA");
        stringList.add("FI");
        stringList.add("FR");
        stringList.add("GA");
        stringList.add("GL");
        stringList.add("GU");
        stringList.add("HE");
        stringList.add("HI");
        stringList.add("HR");
        stringList.add("HT");
        stringList.add("HU");
        stringList.add("ID");
        stringList.add("IS");
        stringList.add("IT");
        stringList.add("JA");
        stringList.add("KA");
        stringList.add("KN");
        stringList.add("KO");
        stringList.add("LT");
        stringList.add("LV");
        stringList.add("MK");
        stringList.add("MR");
        stringList.add("MS");
        stringList.add("MT");
        stringList.add("NL");
        stringList.add("NO");
        stringList.add("PL");
        stringList.add("PT");
        stringList.add("RO");
        stringList.add("RU");
        stringList.add("SK");
        stringList.add("SL");
        stringList.add("SQ");
        stringList.add("SV");
        stringList.add("SW");
        stringList.add("TA");
        stringList.add("TE");
        stringList.add("TH");
        stringList.add("TL");
        stringList.add("TR");
        stringList.add("UK");
        stringList.add("UR");
        stringList.add("VI");
        stringList.add("ZH");
        return stringList;
    }
}